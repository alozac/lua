patterns = {}


function strtable(t, depth)
	depth = depth or 0
	local tab = 
		function (dpth) 
			local s=""
			for i=1,dpth do s = s.."  "end
			return s
		end
	local str= tab(depth).."{ "
	for i,v in ipairs(t) do
		str = str..(type(v) == "table"
					and "\n"..strtable(v, depth+1) 
					or v)
				 ..(i~=#t and ", " or "")
	end
	return type(t[#t])=="table" and str.."\n"..tab(depth).."}" or str.." }" 
end

function split(s, sep)
    local fields = {}

    local sep = sep or " "
    local pattern = string.format("([^%s]+)", sep)
    string.gsub(s, pattern, function(c) fields[#fields + 1] = tonumber(c) end)

    return fields
end

local f = io.open("patterns", "r")
if f == nil then
	print("Pas de fichier")
else
	io.close(f)
	local currPat
	for line in io.lines("patterns") do
		if string.find(line, "[#\n\t ]") or #line==0 then
			if currPat then 
				table.insert(patterns, currPat)
				currPat = nil
			end
		else 
			if currPat == nil then 
				currPat = {}
			end
			table.insert(currPat, split(line, ","))
		end
	end
	if currPat then 
		table.insert(patterns, currPat)
	end
end