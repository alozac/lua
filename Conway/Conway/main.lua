function love.load()
	require("constants")
	require("patterns")

	widthCase  = 15

	nbCaseW = math.floor(SCRWIDTH / widthCase) 
	nbCaseH = math.floor(SCRHEIGHT / widthCase)

	array 	 = {}

	for i=1,nbCaseW do
		table.insert(array, {})
		for j=1,nbCaseH do
			table.insert(array[i], 0)
		end
	end

	-- for x=1,0.2*nbCaseW*nbCaseH do
	-- 	local i = love.math.random(nbCaseW)
	-- 	local j = love.math.random(nbCaseH)
	-- 	array[i][j] = 1
	-- end

	drawPattern(1, 2, 2)
	drawPattern(1, 46, 23)
	drawPattern(2, 35, 30)

	delay = INIT_DELAY
	nextGene = INIT_DELAY

	arrayTmp = clone(array)

	paused = false
	
end


function drawPattern( pat, x, y )
	local pattern = patterns[pat]
	for i,v in ipairs(pattern) do
		for j,w in ipairs(v) do
			array[x+i-1][y+j-1] = w
		end
	end
end

 
function love.draw()
	for i,v in ipairs(array) do
		for j,w in ipairs(v) do
			if w == 1 then
				love.graphics.circle("fill", (i-1)*widthCase, (j-1)*widthCase,  widthCase/2)
			end
		end
	end
end



function love.update(dt)

	if paused then return end
	nextGene = nextGene - dt
	if nextGene > 0 then
		return
	end

	nextGene = delay
	
	arrayTmp = clone(array)
	
	for i,v in ipairs(arrayTmp) do
		for j,w in ipairs(v) do
			local nbNei = nbNeigh(i, j)
			array[i][j] = w == 1 and ((nbNei == 2 or nbNei == 3) and 1 or 0) 
								 or  (nbNei == 3 and 1 or 0)
		end
	end
end

function nbNeigh( x , y )
	local cter = 0
	for i=-1,1 do
		for j=-1,1 do
			if (i~=0 or j~=0) and x+i > 1 and x+i <= nbCaseW and y+j > 1 and y+j <= nbCaseH and arrayTmp[x+i][y+j] == 1 then
				cter = cter + 1
			end
		end
	end
	return cter
end

function clone (t) 
    local meta = getmetatable(t)
    local target = {}
    for k, v in pairs(t) do
        if type(v) == "table" then
            target[k] = clone(v)
        else
            target[k] = v
        end
    end
    setmetatable(target, meta)
    return target
end


function love.keypressed(key, scancode, isrepeat)
	if key == "right" then
		delay = math.max(0.05, delay - 0.05)
	elseif key == "left" then
		delay = delay + 0.05
	elseif key == "q" then 
		love.event.quit()
	elseif key == "space" then
		paused = not paused
	end
end

function love.mousemoved( x, y, dx, dy, istouch )
	if love.mouse.isDown(1) then
		newX, newY = math.ceil(x/widthCase), math.ceil(y/widthCase)
		if newX ~= oldX or newY ~= oldY then
			value = array[newX][newY]
			array[newX][newY] = value == 0 and 1 or 0
			oldX, oldY = newX, newY
		end
	end
end

function love.mousepressed( x, y, button, istouch )
	newX, newY = math.ceil(x/widthCase), math.ceil(y/widthCase)
	value = array[newX][newY]
	array[newX][newY] = value == 0 and 1 or 0
end 

function love.keyreleased(key, scancode, isrepeat)
end