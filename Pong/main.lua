function love.load()
	require("constants")
	require("powerups")

	paused 		= true
	gameStarted = false

	soloMode = true

	local padWidth, padHeight = INIT_PAD_WIDTH, INIT_PAD_HEIGHT

   lpad = {
   			x = MARGIN,
   			y = (SCRHEIGHT-padHeight)/2,
   			width = padWidth,
   			height = padHeight,
   			speed = INIT_PAD_SPEED,
   			startPos = {x = MARGIN,
   						y = (SCRHEIGHT - padHeight)/2 },
   			moving = 0,
   		}
   rpad = {
   			x = SCRWIDTH - MARGIN - padWidth,
   			y = (SCRHEIGHT-padHeight)/2,
   			width = padWidth,
   			height = padHeight,
   			speed = INIT_PAD_SPEED,
     		startPos = {x = SCRWIDTH - MARGIN - padWidth,
     					y = (SCRHEIGHT - padHeight) / 2},
     		moving = 0,
   		}
   ball = {
   			x = SCRWIDTH  / 2,
   			y = SCRHEIGHT / 2,
   			vx = 1,
   			vy = 0,
   			rad = INIT_BALL_RAD,
   			speed = INIT_BALL_SPEED,
   			startPos = {x = SCRWIDTH  / 2,
   						y = SCRHEIGHT / 2},
   			isVisible = true
   		}

   	pads  = { [ID_LPAD] = lpad, [ID_RPAD] = rpad }

   	score = { [ID_LPAD] = 0, [ID_RPAD] = 0}

   	lastPad = nil

   	nextPowerup = POWERUP_DELAY
   	poppedPowerups = {}
   	startedPowerups = {}

end


 
function love.draw()
	love.graphics.setFont(FONT)
	drawText()

	love.graphics.setColor(30, 123, 200)
   
   love.graphics.rectangle("fill", lpad.x, lpad.y, lpad.width, lpad.height)
   love.graphics.rectangle("fill", rpad.x, rpad.y, rpad.width, rpad.height)
   love.graphics.line(SCRWIDTH / 2, 0, SCRWIDTH / 2, SCRHEIGHT)

   if ball.isVisible then
	   love.graphics.circle("fill", ball.x, ball.y, ball.rad)
	end

	drawPowerups()
	
	love.graphics.setColor(255, 255, 255)
end

function drawText()
	if paused then
		local text
		if gameStarted then 
			text = "PAUSE"
		else 
			text = "PRESS START TO PLAY"
		end
		love.graphics.printf(text, 0, 50, SCRWIDTH, "center")
	end

	love.graphics.printf(score[1].."\t\t:\t\t"..score[2], 0, 100, SCRWIDTH, "center")
end

function drawPowerups()
	for i = 1, #poppedPowerups do
		local pow = poppedPowerups[i]
		love.graphics.setColor(pow.color)
		love.graphics.circle("fill", pow.x, pow.y, POWERUP_RAD)
	end
end



function love.update(dt)
	if paused then return end
	movePads()
	if not ballOut() then
		collisionPads()
		collisionEdges()
			if soloMode then updateCPUPad() end
		collisionPowerups()
		ball.x = math.floor(ball.x + ball.speed * ball.vx)
		ball.y = math.floor(ball.y + ball.speed * ball.vy)
	end

	nextPowerup = nextPowerup - dt 
	if nextPowerup <= 0 then
		local new = Powerup:new()
		table.insert(poppedPowerups, new)
		nextPowerup = POWERUP_DELAY
	end

	for i = 1, #startedPowerups do
		local pow = startedPowerups[i]
		if pow:isOver( dt ) then
			table.remove(startedPowerups, i)
			return
		end
	end
end

function updateCPUPad()
	--if not ball.isVisible then return end
	if lpad.y + lpad.height / 2 > ball.y  and ball.vy < 0 then
		lpad.moving = 1
	elseif lpad.y + lpad.height / 2 < ball.y and ball.vy > 0 then
		lpad.moving = -1
	else lpad.moving = 0
	end
end


function movePads()
	if  rpad.moving ~= 0  then
		if rpad.moving == -1 then
			rpad.y = math.min(rpad.y + rpad.speed, SCRHEIGHT - rpad.height)
		else 
			rpad.y = math.max(rpad.y - rpad.speed, 0)
		end
	end
	if lpad.moving ~= 0 then
		if lpad.moving == -1 then 
			lpad.y = math.min(lpad.y + lpad.speed, SCRHEIGHT - lpad.height)
		else  
			lpad.y = math.max(lpad.y - lpad.speed, 0)
		end
	end
end

function collisionEdges()
	if 		ball.y + ball.rad >= SCRHEIGHT
		or (ball.y - ball.rad <= 0 and ball.vy < 0)then
		ball.vy = -ball.vy
	end
end

function collisionPads()

	if 	ball.x + ball.rad >= rpad.x and ball.vx > 0 and
		ball.y - ball.rad <= rpad.y + rpad.height and ball.y + ball.rad >= rpad.y
		or 
		ball.x - ball.rad <= lpad.x + lpad.width and ball.vx < 0 and
		ball.y - ball.rad <= lpad.y + lpad.height and ball.y + ball.rad >= lpad.y
		then 
			
		local posOnPad, newAngle
		if 	ball.vx > 0 then
			posOnPad = (ball.y - rpad.y - rpad.height / 2)
			newAngle = 180 - (90 - MAX_ANGLE) * posOnPad / (rpad.height / 2)
			lastPad = ID_RPAD
		else
			posOnPad = (ball.y - lpad.y - lpad.height / 2)
			newAngle = (90 - MAX_ANGLE) * posOnPad / (lpad.height / 2)
			lastPad = ID_LPAD
		end
		
		ball.vx = math.cos(math.rad(newAngle))
		ball.vy = math.sin(math.rad(newAngle))
		ball.speed = ball.speed + STEP_BALL_SPEED

		HITSOUND:play()
	end
end

function collisionPowerups()
	for i = 1 , #poppedPowerups do
		local pow = poppedPowerups[i]
		local dx = pow.x - ball.x 
		local dy = pow.y - ball.y
		if math.sqrt(dx * dx + dy * dy) <= POWERUP_RAD + ball.rad then
			if pow.isBonus then
				pow.pad = pads[ lastPad ]
			else 
				pow.pad = pads[ lastPad % 2 + 1]
			end
			pow.ball = ball
			pow:start()
			table.insert(startedPowerups, pow)
			table.remove(poppedPowerups, i)
			return
		end 
	end
end

function ballOut()
	if ball.x - ball.rad >= SCRWIDTH or ball.x + ball.rad <= 0 then
		local loser
		if lastPad == ID_LPAD then
			 score[ID_LPAD] = score[ID_LPAD] + 1 ; ball.vx = 1
		else score[ID_RPAD] = score[ID_RPAD] + 1 ; ball.vx = -1
		end

   		ball.vy = 0
   		reset()

   		return true
	end 
	return false
end

function reset()
	paused = true
	gameStarted = false

	ball.x, ball.y = ball.startPos.x, ball.startPos.y
	ball.speed     = INIT_BALL_SPEED
	ball.rad 	   = INIT_BALL_RAD

	rpad.x, rpad.y = rpad.startPos.x, rpad.startPos.y
	lpad.x, lpad.y = lpad.startPos.x, lpad.startPos.y

	rpad.speed, lpad.speed   = INIT_PAD_SPEED, INIT_PAD_SPEED
	rpad.height, lpad.height = INIT_PAD_HEIGHT, INIT_PAD_HEIGHT

	poppedPowerups = {}
	startedPowerups = {}
end


function love.keypressed(key, scancode, isrepeat)
	if key == "space" then
		paused = not paused
		if not gameStarted then
			gameStarted = true
		else ball.isVisible = false
		end
		if not paused then ball.isVisible = true end
	elseif key == "escape" or key == "q" then
		love.event.quit()
	elseif  key == "down" then
		rpad.moving = -1
	elseif  key == "up"   then
		rpad.moving = 1
	elseif  key == "s"  and not soloMode then
		lpad.moving = -1
	elseif  key == "z"  and not soloMode then
		lpad.moving = 1
	elseif  key == "m"    then
		if soloMode then lpad.moving = 0 end
		soloMode = not soloMode
	end
end

function love.keyreleased(key, scancode, isrepeat)
	if     key == "down" or  key == "up"   then
		rpad.moving = 0
	elseif key == "s"    or  key == "z"    then
		lpad.moving = 0
	end
end