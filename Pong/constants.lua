love.math.setRandomSeed(love.timer.getTime())

FONT      = love.graphics.newFont("fonts/font.ttf", 30);
HITSOUND  = love.audio.newSource("sounds/hit.wav", "static")

SCRWIDTH  = love.graphics.getWidth()
SCRHEIGHT =  love.graphics.getHeight()

MAX_ANGLE = 30

MARGIN    = 5

INIT_BALL_RAD   = 7
STEP_BALL_SPEED = 0.2
INIT_BALL_SPEED = 7

INIT_PAD_SPEED  = 10
INIT_PAD_WIDTH  = 15
INIT_PAD_HEIGHT = SCRHEIGHT / 5

ID_LPAD = 1
ID_RPAD = 2

POWERUP_RAD     = 6
POWERUP_DELAY   = 5

