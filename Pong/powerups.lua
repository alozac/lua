local malus = {255, 0, 0}
local bonus = {0, 255, 0}

local function initPos()
	return  love.math.random(30, SCRWIDTH  - 30), 
			love.math.random(10, SCRHEIGHT - 10)
end

local function reduceHeight()
	local ratio = 1.5
	local tmp
	return 4,
	function ( self )
		tmp = self.pad.height
		self.pad.height = self.pad.height / ratio
		tmp = (tmp - self.pad.height) / 2
		self.pad.y = self.pad.y + tmp
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.pad.height = self.pad.height * ratio
				self.pad.y = self.pad.y - tmp
				return true
			end 
			return false
	end,
	malus,
	false
end

local function increaseHeight()
	local ratio = 1.5
	local tmp
	return 4,
	function ( self )
		tmp = self.pad.height
		self.pad.height = self.pad.height * ratio
		tmp = (self.pad.height - tmp) / 2
		self.pad.y = self.pad.y - tmp
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.pad.height = self.pad.height / ratio
				self.pad.y = self.pad.y + tmp
				return true
			end 
			return false
	end,
	bonus,
	true
end

local function reduceSpeedPad()
	local ratio = 2.5
	return 4,
	function ( self )
		self.pad.speed = self.pad.speed / ratio
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.pad.speed = self.pad.speed * ratio
				return true
			end 
			return false
	end,
	malus,
	false
end

local function increaseSpeedPad()
	local ratio = 2
	return 4,
	function ( self )
		self.pad.speed = self.pad.speed * ratio
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.pad.speed = self.pad.speed / ratio
				return true
			end 
			return false
	end,
	bonus,
	true
end

local function reduceSpeedBall()
	local ratio = 2
	return 4,
	function ( self , ball)
		self.ball.speed = self.ball.speed / ratio
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.ball.speed = self.ball.speed * ratio
				return true
			end 
			return false
	end,
	bonus,
	true
end

local function increaseSpeedBall()
	local ratio = 1.25
	return 4,
	function ( self )
		self.ball.speed = self.ball.speed * ratio
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.ball.speed = self.ball.speed / ratio
				return true
			end 
			return false
	end,
	malus,
	false
end

local function reduceSizeBall()
	local ratio = 2
	return 7,
	function ( self , ball)
		self.ball.rad = self.ball.rad / ratio
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.ball.rad = self.ball.rad * ratio
				return true
			end 
			return false
	end,
	malus,
	true
end

local function increaseSizeBall()
	local ratio = 2
	return 7,
	function ( self , ball)
		self.ball.rad = self.ball.rad * ratio
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.ball.rad = self.ball.rad / ratio
				return true
			end 
			return false
	end,
	bonus,
	true
end

local function hideBall()
	return 1,
	function ( self , ball)
		self.ball.isVisible = false
	end ,
	function (self, dt )
		self.duration = self.duration - dt
			if self.duration <= 0 then
				self.ball.isVisible = true
				return true
			end 
			return false
	end,
	malus,
	false
end


local tostring = function(obj)
	s=""
	for k,v in pairs(obj) do
		s = s.."( "..k.." : "
		if type(v) == "number" then
			s = s..v..")  ,  "
		else s = s..type(v)..")  ,  "
		end 
	end
	return s
end



Powerup = {}

local listPow = {reduceHeight,    increaseHeight,
				 reduceSpeedPad,  increaseSpeedPad,
				 reduceSpeedBall, increaseSpeedBall,
				 reduceSizeBall,  increaseSizeBall,
				 hideBall
				}


function Powerup:new()
	local x, y = initPos()
	local rd = love.math.random(1, #listPow)
	local duration, start, isOver, color, isBonus = listPow[rd]()
	local new = {
		x = x ,y = y,
		duration = duration,
		start = start,
		isOver = isOver,
		color = color,
		isBonus = isBonus
		}
	setmetatable(new, self)
	self.__index = self
	self.__tostring = tostring
	return new
end