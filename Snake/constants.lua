love.math.setRandomSeed(love.timer.getTime())

FONT      = love.graphics.newFont("fonts/font.ttf", 30);

SCRWIDTH  = love.graphics.getWidth()
SCRHEIGHT =  love.graphics.getHeight()


STEP_SPEED = 0.1
INIT_SPEED = 0.5

WIDTH = 10

POWERUP_RAD     = 6
POWERUP_DELAY   = 5

DIR_LEFT  = 1
DIR_RIGHT = 2
DIR_UP    = 3
DIR_DOWN  = 4
