function love.load()
	require("constants")

	love.graphics.setFont(FONT)

	paused = true
	gameStarted = false

	snake = {   { x = SCRWIDTH / 2, y = SCRHEIGHT / 2, dir = DIR_LEFT},
				{ x = SCRWIDTH / 2 + WIDTH, y = SCRHEIGHT / 2, dir = DIR_LEFT},
				{ x = SCRWIDTH / 2 + 2*WIDTH, y = SCRHEIGHT / 2, dir = DIR_LEFT} }

	pivots = {}

	nextMove = INIT_SPEED

end


 
function love.draw()
	drawText()
	for _,v in ipairs(snake) do
		love.graphics.rectangle("fill", v.x, v.y, WIDTH, WIDTH)
	end
end

function drawText()
	if paused then
		local text
		if gameStarted then 
			text = "PAUSE"
		else 
			text = "PRESS START TO PLAY"
		end
		love.graphics.printf(text, 0, 50, SCRWIDTH, "center")
	end
end



function love.update(dt)
	if paused then return end
	nextMove = nextMove - dt
	if nextMove <= 0 then 
		nextMove = INIT_SPEED
		moveSnake()
		
	end 
end

function moveSnake()
	for _, v in ipairs(snake) do
		local mvx = 0
		local mvy = 0
		if 	   v.dir == DIR_LEFT then
			 mvx = -1
		elseif v.dir == DIR_RIGHT then
			 mvx = 1
		elseif v.dir == DIR_UP then
			 mvy = -1
		else mvy = 1 
		end
		v.x = v.x + WIDTH * mvx
		v.y = v.y + WIDTH * mvy
	end
end

function reset()
	paused = true
	gameStarted = false
end


function love.keypressed(key, scancode, isrepeat)
	if key == "space" then
		paused = not paused
		if not gameStarted then
			gameStarted = true
		end
	elseif key == "escape" or key == "q" then
		love.event.quit()
	elseif  key == "down" or key == "up" or key == "right" or key == "left" then
		local head = snake[1]
		if 	   key == "down" then head.dir = DIR_DOWN
		elseif key == "up"   then head.dir = DIR_UP
		elseif key == "right"then head.dir = DIR_RIGHT
		else 					  head.dir = DIR_LEFT
		end
		table.insert(pivots, head)

		for _,v in ipairs(snake) do
			print(v.dir)
		end
	end
end