function love.load()
	require("constants")
	require("blocks")
	require("scores")

	love.graphics.setLineStyle("rough")

	canvas = love.graphics.newCanvas()
    love.graphics.setCanvas(canvas)
	initCanvas()
    love.graphics.setCanvas()

    resetGame()
end

function resetGame()
	paused      = true
	gameStarted = false
	endGame 	= false
	endDrawn    = false

	nextMove 	   = INIT_STEP_MOVE
	nextPerformKey = INIT_PERFORM_KEY
	moving 		   = NONE

	board      = {}
	colorBoard = {}
	initBoard()

	pieceAlive = false
	infosPiece = { color = {0, 0, 0}}
	piece 	   = nil
	nextPiece  = getRandomPiece()

	score = 0
	level = 1
	nextLevel = INIT_NEXT_LVL
	stepMove  = INIT_STEP_MOVE

	timeDownPressed = 0
end

function initBoard()
	for i = 1, NB_BLK_H do
		table.insert(board, {})
		table.insert(colorBoard, {})
		for j = 1, NB_BLK_W do 
			table.insert(board[i], FREE)
			table.insert(colorBoard[i], nil)	
		end
	end 
end

function initCanvas()
	setFont(secondFont)

	drawRect("fill" , BOARD_POSX, BOARD_POSY     , BOARD_WIDTH, BOARD_HEIGHT)

	printf  ("NEXT" , NEXT_POSX , NEXT_POSY - 20 , NEXT_WIDTH , "center")
	drawRect("fill" , NEXT_POSX , NEXT_POSY      , NEXT_WIDTH , NEXT_HEIGHT , RND_CORNER)

	printf  ("SCORE", SCORE_POSX, SCORE_POSY - 20, SCORE_WIDTH, "center")
	drawRect("fill" , SCORE_POSX, SCORE_POSY     , SCORE_WIDTH, SCORE_HEIGHT, RND_CORNER)

	printf  ("LEVEL", LVL_POSX  , LVL_POSY - 20  , LVL_WIDTH  , "center")
	drawRect("fill" , LVL_POSX  , LVL_POSY       , LVL_WIDTH  , LVL_HEIGHT  , RND_CORNER)

	setColor(0, 0, 0, 50)
	for i = BOARD_POSX, NB_BLK_W * BLOCK_SIZE, BLOCK_SIZE do
		love.graphics.line(i, BOARD_POSY, i, BOARD_POSY+BOARD_HEIGHT)
	end
	for j = BOARD_POSY, NB_BLK_H * BLOCK_SIZE, BLOCK_SIZE do 
		love.graphics.line(BOARD_POSX, j, BOARD_POSX + BOARD_WIDTH, j)
	end
end
 




function love.draw()
	love.graphics.draw(canvas)
	
	drawBoard()
	drawNextPiece()
	drawText()
	if endGame then
		drawEndGame()
	end

	setColor(255, 255, 255)
end

function drawBoard()
	for i = 0, NB_BLK_W - 1 do
		for j = 4, NB_BLK_H - 1 do 	
			if (board[j + 1][i + 1] ~= FREE) then
				setColor(colorBoard[j + 1][i + 1])
				drawRect("fill", BOARD_POSX + BLOCK_SIZE * i,
								 BOARD_POSY + BLOCK_SIZE * (j-4), BLOCK_SIZE, BLOCK_SIZE)
			end
		end
	end
end

function drawNextPiece()
	for i = 0, 3 do
		for j = 0, 3 do 	
			if nextPiece.piece[j+1][i+1] ~= FREE then
				setColor(nextPiece.color)
				drawRect("fill", NEXT_POSX + BLOCK_SIZE * i,
								 NEXT_POSY + BLOCK_SIZE * j, BLOCK_SIZE, BLOCK_SIZE)
			end
		end
	end
end

function drawText()
	setFont(scoreFont)
	setColor(infosPiece.color)
	printf(score, SCORE_POSX, SCORE_POSY + SCORE_HEIGHT/2 - 15, SCORE_WIDTH, "center")

	setColor(0, 0, 0)
	printf(level, LVL_POSX, LVL_POSY + LVL_HEIGHT/2- 10, LVL_WIDTH, "center")

	if paused then
		setFont(mainFont)
		local text
		if gameStarted then 
			text = "PAUSE"
		else
			text = "PRESS START TO PLAY"
		end
		printf(text, BOARD_POSX, BOARD_POSY + BOARD_HEIGHT/4, BOARD_WIDTH, "center")
	end

	setColor(255, 255, 255)
	setFont(infosFont)
	printf('space:    pause\nq:      quit\nr: restart',
			SCORE_POSX, SCRHEIGHT - 50, SCORE_WIDTH, "right")
end

function drawEndGame()
	setColor(255, 255, 255, 225)
	drawRect("fill", END_WIN_POSX, END_WIN_POSY, END_WIN_W, END_WIN_H, RND_CORNER)
	
	setColor(0, 0, 0)
	setFont(mainFont)
	printf("YOU LOST !", END_WIN_POSX, END_WIN_POSY + 20, END_WIN_W, "center")
	setFont(secondScoreFont)
	printf("Your score : \n"..tostring(score), END_WIN_POSX, END_WIN_POSY + 70, END_WIN_W, "center")

	printf("H i g h s c o r e s", END_WIN_POSX, END_WIN_POSY + 125, END_WIN_W, "center")
	if not endDrawn then updateHighscores(score) end
	local padding = 25/100 * END_WIN_W

	for i = 1 , #highscores do
		if highscores[i] == score then
			setColor(255, 0, 0)
		else setColor(0, 0, 0) end

		local posY = END_WIN_POSY + 130 + i*25
		printf(        tostring(i)       , END_WIN_POSX + padding, posY , END_WIN_W - 2*padding, "left")
		printf("........................", END_WIN_POSX + padding, posY , END_WIN_W - 2*padding, "center")
		printf(tostring(highscores[i])   , END_WIN_POSX + padding, posY , END_WIN_W - 2*padding, "right")
	end
	endDrawn = true
end





function love.update( dt )
	if paused then return end

	if not pieceAlive then
		if endGame or hasLost() then endGame = true; return end
		addNextPiece()
		pieceAlive = true
		nextMove   = INIT_STEP_MOVE
		return
	end

	if moving ~= NONE then
		nextPerformKey = nextPerformKey - dt
		if moving == DOWN then timeDownPressed = timeDownPressed + dt end
		if nextPerformKey <= 0 then 
			nextPerformKey = INIT_PERFORM_KEY
			move(moving)
			return
		end
	end

	nextMove = nextMove - dt
	if nextMove <= 0 then
		nextMove = stepMove
		move(DOWN)
	end
end

function addNextPiece()
	piece, infosPiece.typeP, infosPiece.rot = nextPiece.piece, nextPiece.typeP, nextPiece.rot
	infosPiece.color = nextPiece.color
	nextPiece = getRandomPiece()
	for i = 1, 4 do
		for j = 1, 4 do
			board[i][j + NB_BLK_W/2-2] = piece[i][j]
		end
	end
	infosPiece.x = NB_BLK_W/2-1
	infosPiece.y = 1
end

function hasLost()
	for i = 1, NB_BLK_W do
		if board[4][i] ~= FREE then return true end
	end
	return false
end

function lockPiece()
	for i = 0, 3 do
		for j = 0, 3 do
			if piece[i+1][j+1] == ALIVE then
				board[i + infosPiece.y][j + infosPiece.x] = LOCKED end
		end
	end
end

function clearPiece( )
	for i = 0, 3 do
		for j = 0, 3 do
			if piece[i+1][j+1] == ALIVE then
				board     [i + infosPiece.y][j + infosPiece.x] = FREE
				colorBoard[i + infosPiece.y][j + infosPiece.x] = nil
			end
		end
	end
end

function fullLine()
	local b
	local nbLines = 0
	for i = infosPiece.y, math.min(infosPiece.y + 3, NB_BLK_H) do
		b = false
		for j = 1, NB_BLK_W do
			if board[i][j] == FREE then b = true; break end
		end
		if not b then
			nbLines = nbLines + 1
			table.remove(board, i)
			table.remove(colorBoard, i)
			table.insert(board, 1, {})
			table.insert(colorBoard, 1, {})
			for j = 1, NB_BLK_W do 
				table.insert(board[1], FREE)
				table.insert(colorBoard[1], nil)
			end
		end
	end
	updateScore(nbLines)
end

function updateScore( nbLines )	
	score = score + math.floor(timeDownPressed * level * COEF_DOWN_PRESSED)
				  + scoreByLines[nbLines] * level
	timeDownPressed = 0
	nextLevel =  nextLevel - nbLines
	if nextLevel <= 0 then 
		level     = level + 1
		nextLevel = INIT_NEXT_LVL
		stepMove  = math.max(stepMove - STEP_INCR_LVL, MAX_STEP_MOVE)
	end
end


function canMove(dir)
	local mvx, mvy, inBoard
	if     dir == DOWN  then
		 mvx, mvy = 0, 1
	elseif dir == RIGHT then 
		 mvx, mvy = 1, 0
	else mvx, mvy = -1, 0
	end
	for i = 0, 3 do
		for j = 0, 3 do
			if  piece[i + 1][j + 1] == ALIVE then
				if not isInBoard(j + infosPiece.x + mvx, i + infosPiece.y + mvy) 
					or board[i + infosPiece.y + mvy][j + infosPiece.x + mvx] == LOCKED then
					return false
				end
			end
		end
	end
	return true, mvx, mvy
end

function move( dir )
	local can, mvx, mvy = canMove(dir)
	if not can then 
		if dir == DOWN then 
			pieceAlive = false
			lockPiece()
			fullLine()
		end
		return
	end
	clearPiece()
	for i = 0, 3 do
		for j = 0, 3 do
			if piece[i+1][j+1] == ALIVE then
				board     [i + infosPiece.y + mvy][j + infosPiece.x + mvx] = ALIVE
				colorBoard[i + infosPiece.y + mvy][j + infosPiece.x + mvx] = infosPiece.color
			end
		end
	end
	infosPiece.x = infosPiece.x + mvx
	infosPiece.y = infosPiece.y + mvy
end

function canRotate()
	local new, rot = getNextRot( infosPiece.typeP, infosPiece.rot)
	for i = 0, 3 do
		for j = 0, 3 do
			if  new[i + 1][j + 1] == ALIVE then
				if not isInBoard(j + infosPiece.x, i + infosPiece.y) 
					or board[i + infosPiece.y][j + infosPiece.x] == LOCKED then
					return false
				end
			end
		end
	end
	return true, new, rot
end

function rotate()
	local can, newPiece, newRot = canRotate()
	if not can then return false end
	clearPiece()
	piece, infosPiece.rot = newPiece,newRot
	for i = 0, 3 do
		for j = 0, 3 do
			if piece[i+1][j+1] == ALIVE then
				board     [i + infosPiece.y][j + infosPiece.x] = piece[i + 1][j + 1]
				colorBoard[i + infosPiece.y][j + infosPiece.x] = infosPiece.color
			end
		end
	end
end

function isInBoard(i, j)
	return 		i > 0 and i <= NB_BLK_W
			and j > 0 and j <= NB_BLK_H
end



function love.keypressed(key, scancode, isrepeat)
	if key == "space" then
		paused = not paused
		if not gameStarted then
			gameStarted = true 
		end
	elseif key == "escape" or key == "q" then
		saveScores()
		love.event.quit()
	elseif  key == "right" then
		moving = RIGHT
	elseif  key == "left"  then
		moving = LEFT
	elseif  key == "down" then
		moving = DOWN
	elseif  key == "up"   then
		if not paused then rotate() end
	elseif  key == "r"   then
		resetGame()
	end
end

function love.keyreleased(key, scancode, isrepeat)
	if     key == "right" or key == "left"
		or key == "down" then
		moving = NONE
	end
end
