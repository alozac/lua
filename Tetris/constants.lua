love.math.setRandomSeed(love.timer.getTime())

mainFont   		= love.graphics.newFont("fonts/blocks.ttf", 25)
secondFont 		= love.graphics.newFont("fonts/blocks.ttf", 10)
scoreFont  		= love.graphics.newFont("fonts/blocko.ttf", 40)
secondScoreFont		= love.graphics.newFont("fonts/blocko.ttf", 20)
infosFont 		= love.graphics.newFont("fonts/blocko.ttf", 15 )

drawRect = love.graphics.rectangle
printf   = love.graphics.printf
setColor = love.graphics.setColor
setFont  = love.graphics.setFont

SCRWIDTH  = love.graphics.getWidth()
SCRHEIGHT =  love.graphics.getHeight()

BLOCK_SIZE = 34

INIT_STEP_MOVE   = 0.70
MAX_STEP_MOVE 	 = 0.25
STEP_INCR_LVL 	 = 0.06
INIT_PERFORM_KEY = 0.1
INIT_NEXT_LVL    = 6

NB_BLK_W = 10
NB_BLK_H = 20 + 4

COEF_DOWN_PRESSED = 75
scoreByLines 	  = {[0] = 0, [1] = 40, [2] = 100, [3] = 300, [4] = 1200 }

FREE   = 0
ALIVE  = 1
LOCKED = 2

NONE  = 0
RIGHT = 1
LEFT  = 2
DOWN  = 3

BOARD_WIDTH  = BLOCK_SIZE * NB_BLK_W
BOARD_HEIGHT = BLOCK_SIZE * (NB_BLK_H - 4)
BOARD_POSX   = BLOCK_SIZE
BOARD_POSY   = 10

NEXT_WIDTH  = BLOCK_SIZE * 4
NEXT_HEIGHT = BLOCK_SIZE * 4
NEXT_POSX   = BLOCK_SIZE * (NB_BLK_W + 2)
NEXT_POSY   = BLOCK_SIZE * 3

SCORE_WIDTH  = BLOCK_SIZE * 4
SCORE_HEIGHT = BLOCK_SIZE * 2
SCORE_POSX   = BLOCK_SIZE * (NB_BLK_W + 2)
SCORE_POSY   = BLOCK_SIZE * 10

LVL_WIDTH  = BLOCK_SIZE * 4
LVL_HEIGHT = BLOCK_SIZE * 1
LVL_POSX   = BLOCK_SIZE * (NB_BLK_W + 2)
LVL_POSY   = BLOCK_SIZE * 15

END_WIN_W    = 80/100 * SCRWIDTH
END_WIN_H 	 = 80/100 * SCRHEIGHT
END_WIN_POSX = 10/100 * SCRWIDTH
END_WIN_POSY = 10/100 * SCRHEIGHT

RND_CORNER = 10
