local scores = {}


local f = io.open("scores", "r")
if f == nil then
	local f = io.open("scores", "w")
	io.close(f)
else
	io.close(f)
	for line in io.lines("scores") do
	table.insert(scores, tonumber(line))
	end
end


highscores = {}

function updateHighscores( score )
	highscores = {}
	table.insert(scores, score)
	table.sort(scores, function (a, b)
						return a > b
						end
				)
	for i = 1, 15 do
		if i <= #scores then
			table.insert(highscores, scores[i])
		else table.insert(highscores, 0)
		end
	end
end

function saveScores()
	local f = io.open("scores", "w")
	for _,v in ipairs(scores) do
		f:write(v.."\n")
	end
	io.close(f)
end